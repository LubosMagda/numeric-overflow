'use strict';

const AMOUNT_LENGTH_MAX = 10;

const AMOUNT_MIN = 0;
const AMOUNT_MAX = 1000000000;

const THRESHOLD = 1000;
const SURCHARGE = 10;

class Amount {

    constructor(amountValue) {
        this.value = amountValue;
    }

    /**
     * Validates user input and makes sure it is a string not longer than AMOUNT_LENGTH_MAX.
     * @param usafeStringAmount
     * @returns {*}
     */
    static validateStringAmount(usafeAmount) {
        if (usafeAmount === undefined || usafeAmount === null) {
            throw RangeError('Amount not specified');
        }
        const usafeStringAmount = usafeAmount.toString();
        if (usafeStringAmount.length <= AMOUNT_LENGTH_MAX && /^\d+$/.test(usafeStringAmount)) {
            return usafeStringAmount;
        }
        throw RangeError(`Amount too long: ${usafeStringAmount}`);
    }

    /**
     * Checks if the parsed amount is within acceptable bounds.
     * @param usafeIntAmount Parsed user input
     * @returns {*}
     */
    static validateParsedAmount(usafeIntAmount) {
        if (AMOUNT_MIN < usafeIntAmount && usafeIntAmount <= AMOUNT_MAX) {
            return usafeIntAmount;
        }
        throw RangeError(`Invalid amount: ${usafeIntAmount}`);
    }

    /**
     * Creates immutable amount wrapper from user/unsafe input.
     * @param unsafeAmount User input
     * @returns {Readonly<Amount>}
     */
    static fromUnsafe(unsafeAmount) {
        // Parse unsafe input
        // Note: parseInt must be able to parse AMOUNT_LENGTH_MAX character string into a number without overflow
        const unsafeStringAmount = Amount.validateStringAmount(unsafeAmount);
        const unsafeIntAmount = parseInt(unsafeStringAmount, 10);
        const safeIntAmount = Amount.validateParsedAmount(unsafeIntAmount);
        // Immutable amount wrapper
        return Object.freeze(new Amount(safeIntAmount));
    }

    requiresApproval() {
        return this.value >= THRESHOLD - SURCHARGE;
    }
}

module.exports = Amount;
